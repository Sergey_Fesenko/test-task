jQuery(document).ready(function() {
    jQuery('#footer-contact-modal').on('click', function(e) {
        e.preventDefault();
        jQuery('.footer-contact-modal, .footer-contact-overlay').fadeIn(300);
    });
    jQuery('.footer-contact-overlay').on('click', function() {
        jQuery('.footer-contact-modal, .footer-contact-overlay').fadeOut(300);
    });
});