<?php
add_action( 'wp_enqueue_scripts', 'test_theme_enqueue_styles' );
function test_theme_enqueue_styles() {
    $parent_style = 'twentyseventeen-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    
    wp_enqueue_script(
        'child-script',
        get_stylesheet_directory_uri() . '/assets/js/child.js',
        array( 'jquery' )
    );
}

// Move Add to cart button
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15);

// Move rating
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 30 );

// Remove categories
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );


// Book & Genre
function codex_custom_init() {
    $args = array(
      'public' => true,
      'label'  => 'Books',
      'has_archive' => true
    );
    register_post_type( 'books', $args );
}
add_action( 'init', 'codex_custom_init' );

function create_book_tax() {
	register_taxonomy(
		'genre',
		'books',
		array(
			'label' => __( 'Genre' ),
			'rewrite' => array( 'slug' => 'genre' ),
			'hierarchical' => true,
		)
	);
}
add_action( 'init', 'create_book_tax' );


add_filter( 'manage_books_posts_columns', 'set_custom_edit_book_columns' );
function set_custom_edit_book_columns($columns) {
    $new = array();
    foreach($columns as $key=>$value) {
        if($key=='date') {
            $new['book_genre'] = __( 'Genre', 'twentyseventeen' );
        }
        $new[$key]=$value;
    }
    return $new;
}
add_action( 'manage_books_posts_custom_column' , 'custom_book_column', 10, 2 );
function custom_book_column( $column, $post_id ) {
    switch ( $column ) {
        case 'book_genre' :
            $terms = get_the_term_list( $post_id , 'genre' , '' , ',' , '' );
            if ( is_string( $terms ) )
                echo $terms;
            else
                _e( 'Genre(s) is not selected.', 'twentyseventeen' );
            break;

    }
}